## YOLO dent detection model

### How to

* The training test and validation data set needs to be taken from the ds-dent-detection repo - https://bitbucket.org/claimgeniushq/ds-dent-detection/src/asia_sedan/ 
* Generate the train, test and validation data files using the script - yolo-dent-detection/convert_jasmine_to_yolo.py
```
python convert_jasmine_to_yolo.py # update the source directory and destination directory in the main function
```
* Download the pretrained weights from dvc - yolo-dent-detection/model_data/yolo_weights.h5
* Modify the paths and parameters in - yolo-dent-detection/YOLO_config.json
* Run the training script
```
conda activate eval_mrcnn
python train.py [--config YOLO_config.json]
```
* Once the training is done, you can use the evaluation script from - ds-sandbox-repo/yolo_eval. The evaluation script, configuration and outputs are similar to eval_mrcnn
