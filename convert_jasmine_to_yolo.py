import os
import sys
import json
from glob import glob

import numpy as np

class_names_label = ["dent"]  # for dent

# ###################################################################################
# class_names_label = ["scratch"]  # for scratch model
# class_names_label = ["ch"]  # for tear model

# # for all damage model
# class_names_label = ["dent", "scratch", "tear"]
# ###################################################################################

def load_image_gt(image_name):
    """Load and return ground truth data for an image (image, mask, bounding boxes).
    """
    # Load image and mask
    polygons, class_ids = load_polygon(image_name)
    
    bbox = extract_bbox_from_polygon(polygons)

    return class_ids, bbox, polygons

def extract_bbox_from_polygon(points):
    '''
    Exracting bounding box from list of polygons
    Argument:
        points: [list of polygon points] 
        point -> [[x1, y1], [x2, y2], ...]
    returns:
        bboxes: [list of bounding boxes]
        box-> [xmin, ymin, xmax, ymax]
    '''
    bboxes = []
    for _, point in enumerate(points):
        point = np.array(point)
        ymin, ymax = np.min(point[:, 0]), np.max(point[:, 0])
        xmin, xmax = np.min(point[:, 1]), np.max(point[:, 1])
        bbox = [xmin, ymin, xmax, ymax]
        bbox = [int(item) for item in bbox]
        bboxes.append(bbox)
    return bboxes

def load_polygon(image_name):
    """
    Getting the class_id and the polygon from curated json
    """
    
    class_name_to_id = {}
    class_id = 0
    for class_name in range(0, len(class_names_label)):
        class_name = class_names_label[class_name].strip()
        class_name_to_id[class_name] = class_id
        class_id += 1
    #===============================================================

    label_file = image_name.replace('.jpg', '.json')
    with open(label_file) as f:
        data = json.load(f)
        if len(data['shapes']) != 0:
            polygons, lbl = shapes_to_label(shapes=data['shapes'], 
                                            label_name_to_value=class_name_to_id)
        else: 
            polygons = np.array([])
            lbl = np.array([])    
    return polygons, lbl.astype(np.int32)

def shapes_to_label(shapes, label_name_to_value):
    polygons = []
    class_id = []
    for shape in shapes:
        polygons.append(shape['points'])
        cls_name = shape['label']
        cls_id = label_name_to_value[cls_name]
        class_id.append(cls_id)

    class_id = np.asarray(class_id)
    return polygons, class_id

if __name__ == "__main__":
    source_dir = '/home/paperspace/renjith/dent_asia_sedan/ds-dent-detection/data/validation_data'
    dest_dir = './data/'

    images_list = glob(source_dir+"/*.jpg")
    # iterating through all the images in the dataset
    total_images = len(images_list)
    count = 1
    yolo_labels = []
    for image_name in images_list:
        # sys.stdout.write("predicting {}/{}...\r".format(count, total_images))
        # sys.stdout.flush()
        count += 1
        
        # reading the image ground truth data
        gt_class_ids, gt_bbox, _ = load_image_gt(image_name)
        
        # saving the curation details in necessary format to yolo format file
        gt = {}
        gt['img_path'] = image_name
        gt['label'] = ""
        labels = []
        
        #TODO: Take care of the very small polygon scenarios
        for class_id, bbox in zip(gt_class_ids, gt_bbox):
            label = ",".join([str(int(bbox[1])), 
                              str(int(bbox[0])), 
                              str(int(bbox[3])), 
                              str(int(bbox[2])),
                              str(class_id)])
            labels.append(label)
        gt['label'] = " ".join(labels)
        gt = " ".join([gt['img_path'], gt['label']])
        yolo_labels.append(gt)
        
    target_text_file = os.path.join(dest_dir, os.path.basename(source_dir)+'.txt')
    print("Saving the result to {}".format(target_text_file))
    with open(target_text_file, 'w') as f:
        for item in yolo_labels:
            f.write("%s\n" % item)
