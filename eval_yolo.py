import sys
import os
import argparse
import json
import time
from glob import glob
from copy import deepcopy

import pandas as pd
import PIL
from PIL import Image
import numpy as np 

from yolo import YOLO, draw_label
import utils

print_separator = lambda x="#": print(str(x)*100)

def predict_img(yolo_model, input_img_path, output_img_path):
    try:
        image = Image.open(input_img_path)
    except FileNotFoundError:
        print('Open Error for image {}! Try again!'.format(input_img_path))
        return False
    else:
        result_image = yolo_model.detect_image(image)
        result_image.save(output_img_path)
    return True

def predict_on_directory(yolo_model, input_path, output_path, class_names):
    input_imgs = glob(input_path+"/*.jpg")
    total_imgs = len(input_imgs)
    count = 0
    print("# {} images to process".format(total_imgs))
    for input_img_path in input_imgs:
        sys.stdout.write("Predicting {}/{}...\r".format(count, total_imgs))
        sys.stdout.flush()
        count += 1
        try:
            image = Image.open(input_img_path)
        except FileNotFoundError:
            print('Open Error for image {}!'.format(input_img_path))
        else:
            r = yolo_model.detect(image)
            result_image = draw_label(image, r, class_names)
            final_result = get_formatted_result(r, image, input_img_path)
            output_img_path = os.path.join(output_path, os.path.basename(input_img_path))
            result_image.save(output_img_path)
            
            with open(output_img_path.replace('.jpg', '.json'), 'w') as f:
                json.dump(final_result, f, indent=2, cls=NumpyEncoder)

class NumpyEncoder(json.JSONEncoder):
    """
    Extending the josn.JSONEncoder class to customise the data before writing to json
    """
    def default(self, obj):
        """
        Ensures object to be written is numpy array 
        converts it to list before serializing
        """
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        
        # else raises a type error if input is not a numpy array saying it is not JSON serializable
        return json.JSONEncoder.default(self, obj)

def get_formatted_result(result, image, input_img_path):
    final_result = {}
    final_result['class_ids'] = np.array(result['class_ids'])
    final_result['scores'] = np.array(result['scores'])
    final_result['rois'] = np.array(result['rois'])
    final_result['points'] = np.array(result['points'])
    final_result['image_dimensions'] = np.array([image.size[1],
                                            image.size[0], 
                                            len(result['class_ids'])])
    final_result['image_location'] = os.path.basename(input_img_path)
    return final_result
            
def load_config(config_path):
    '''
    Loads the yolo configuration for training or evalaution
    '''
    with open(config_path, 'r') as f:
        config = json.load(f)
    
    return config

def display_config(config):
    print_separator("*")
    print("#"*39, " Configuration File ", "#"*39)
    print_separator("*")
    print(json.dumps(config, indent=2, sort_keys=False))
    print_separator("*")

def load_args():
    # class YOLO defines the default value, so suppress any default here
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    
    default_input = "./sample/input/1.jpg"
    default_output = "./sample/output/temp.jpg"
    default_config = "./YOLO_config.json"
    
    parser.add_argument(
        "--input", nargs='?', type=str, required=False, default=default_input,
        help="Image input path, default {}".format(default_input)
    )

    parser.add_argument(
        "--output", nargs='?', type=str, required=False, default=default_output,
        help="Image output path, default {}".format(default_output)
    )
    
    parser.add_argument(
        "--config", nargs='?', type=str, required=False, default=default_config,
        help="Configuration file path, default {}".format(default_config)
    )
    return parser.parse_args()

def update_args(args, config):
    class_type = config['yolo']['class_type']
    args['model_path'] = config['model_params'][class_type]['model_path']
    args['anchors_path'] = config['model_params'][class_type]['anchors_path']
    args['classes_path'] = config['model_params'][class_type]['classes_path']
    args['score'] = config['config_params']['eval_score_thresh']
    args['iou'] = config['yolo']['IoU_threshold']
    args['model_image_size'] = (config['config_params']['input_shape'], 
                                config['config_params']['input_shape'])
    
    return args

def get_evaluation_mode(config):
    eval_mode = None
    print_separator()
    if config['evaluation']['precision_recall_calculation']:
        print("Precision Recall calculation mode...")
        eval_mode = 0
    elif config['evaluation']['complete_evaluation']:
        print("Complete model evaluation mode...")
        eval_mode = 1
    elif config['evaluation']['prediction_on_images']:
        print("Entire directory image prediction mode...")
        eval_mode = 2
    elif config['evaluation']['single_image_prediction']:
        print("Single image prediction mode...")
        eval_mode = 3
    else:
        print("Incorrect configuration in config['evaluation']!")
    print_separator()
    return str(eval_mode)

def generate_data(yolo_model, input_path, output_path, class_names):
    ## =================== Create or empty required folder=============================#
    required_folder = ['predicted', 'curated', 'raw_images', 
                       'masked_images_predicted', 'masked_images_curated']
    for folder in required_folder:
        if os.path.exists(os.path.join(output_path, folder)):
            all_files = os.listdir(os.path.join(output_path, folder))
            for file in all_files:
                os.remove(os.path.join(output_path, folder, file))
        else:
            os.makedirs(os.path.join(output_path, folder))
	#====================== Folder created or Emptied==================================#
 
    input_imgs = glob(input_path+"/*.jpg")
    total_imgs = len(input_imgs)
    count = 0
    print("# {} images to process".format(total_imgs))
    for input_img_path in input_imgs:
        sys.stdout.write("Predicting {}/{}...\r".format(count, total_imgs))
        sys.stdout.flush()
        count += 1
        try:
            image = Image.open(input_img_path)
            act_img = deepcopy(image)
        except FileNotFoundError:
            print('Open Error for image {}!'.format(input_img_path))
        else:
            r = yolo_model.detect(image)
            final_result = get_formatted_result(r, image, input_img_path)
            
            #============ Saving the raw image ========================================#
            raw_img_path = os.path.join(output_path, 
                                           'raw_images', 
                                           os.path.basename(input_img_path))
            act_img.save(raw_img_path)
            
            # # generating the labelled image
            # predicted_image = draw_label(image, r, class_names)
            
            # # #========== Saving the predicted image ====================================#
            # output_img_path = os.path.join(output_path, 
            #                                'masked_images_predicted', 
            #                                os.path.basename(input_img_path))
            # predicted_image.save(output_img_path)
            
            #========== Saving the predicted json =====================================#
            predicted_json_path = os.path.join(output_path, 
                                           'predicted', 
                                           os.path.basename(input_img_path).replace('.jpg', '.json'))
            
            with open(predicted_json_path, 'w') as f:
                json.dump(final_result, f, indent=2, cls=NumpyEncoder)
        
        try:
            class_ids, rois = load_image_gt(input_img_path, class_names)
        except:
            print('Open Error for json {}!'.format(input_img_path.replace('.jpg', '.json')))
        else:
            gt_points = []
            for _, roi in enumerate(rois):
                ymin, xmin, ymax, xmax = roi[0], roi[1], roi[2], roi[3]
                gt_point = [[xmin, ymin], [xmax, ymin], [xmax, ymax], [xmin, ymax]]
                gt_points.append(gt_point)
            gt = {'class_ids':np.array(class_ids)+1, 
                  'scores': np.ones(len(class_ids)), 
                  'rois': np.array(rois), 
                  'points': np.array(gt_points),
                  'image_location' : os.path.basename(input_img_path),
                  'image_dimensions': np.array([image.size[1],
                                                image.size[0],
                                                len(class_ids)])}
            #========== Saving the curated json =====================================#
            curated_json_path = os.path.join(output_path,
                                             'curated', 
                                             os.path.basename(input_img_path).replace('.jpg', '.json'))
            
            with open(curated_json_path, 'w') as f:
                json.dump(gt, f, indent=2, cls=NumpyEncoder)
                
            #========== Saving the curated masked image =============================#
            curated_img_path = os.path.join(output_path,
                                             'masked_images_curated', 
                                             os.path.basename(input_img_path))
            # generating the labelled image
            curated_image = draw_label(act_img, gt, class_names)
            curated_image.save(curated_img_path)

def evaluation(threshold, class_names, output_path, IoU_threshold, save_masked_image=True):
    """
    Calculates the performance metrics from the model predictions
    Masked_images_curated and masked_images_predicted are generated during this function call
    
    Inputs:
        threshold - The DETECTION_MIN_CONFIDENCE threshold for detected polygons to be counted as matching
        class_names - list of classes that needs to be predicted 
        output_path - Folder that contains predicted json, curated json and raw images
    Returns:
        final_df - Dataframe containing precision, recall, and accuracy 
                   The calculated performance metrics are returned 
                   so that it can be sent to neptune from main function
    """
    if class_names[0] != 'BG':
        class_names.insert(0, 'BG')
    ## ================= Create empty dictionary with all class names ===============##
    class_wise_data = dict()
    
    # index starting from 1 to avoid background(BG) class
    for c in class_names[1:]:  
        class_wise_data["curated_"+str(c)] = 0
        class_wise_data["predicted_"+str(c)] = 0
        class_wise_data["overlap_curated_"+str(c)] = 0
        class_wise_data["overlap_predicted_"+str(c)] = 0
    predicted_data = dict()
    curated_data = dict()
    ## ========================= Read all files predicted ============================##
    all_files = os.listdir(os.path.join(output_path, "predicted"))
    
    ################################################################
    # A file to store the image wise results
    # Useful when calculating positionwise metrics
    file_wise_data_fname = os.path.join(output_path, "file_wise_data_"+str(threshold)+".csv")
    try:
        os.remove(file_wise_data_fname)
    except FileNotFoundError:
        pass
    
    ## ========================= Read all files one by one ===========================##
    file_count = 1
    total_files = len(all_files)
    for file in all_files:
        sys.stdout.write("evaluating {}/{}...\r".format(file_count, total_files))
        sys.stdout.flush()
        # initializing a dictionary for keeping track of image wise data
        file_wise_data = dict()
        file_wise_data["curated"] = 0
        file_wise_data["predicted"] = 0
        file_wise_data["overlap_polygons"] = 0
        file_wise_data["non_overlapping_polygons"] = 0
        file_wise_data["file_name"] = file.strip(".json")
        
        ##===================== Reading Curated json  =============================##
        with open(os.path.join(output_path, "curated", file)) as json_file:
            curated_data = json.load(json_file)
        
        for gt in curated_data['class_ids']:
            class_wise_data['curated_'+class_names[int(gt)]] += 1
            file_wise_data["curated"] += 1
        
        # Generating masks from polygons
        curated_data['masks'], points_to_remove_ = generate_mask_from_polygon(curated_data['points'], 
                                                                              curated_data['image_dimensions'][:2])
        
        # When some some file with 2 point polygon curated encounters
        assert len(points_to_remove_) == 0, "Curated file {} has 2 point polygon!".format(file)
        
        ##===================== Reading Predicted json  =========================##
        with open(os.path.join(output_path, "predicted", file)) as json_file:
            data = json.load(json_file)

        length = len(data['scores'])
        if length > 0:
            predicted_data['image_dimensions'] = data['image_dimensions']
        class_ids = []
        rois = []
        scores = []
        points = []
        
        for i in range(0, len(data['scores'])):
            if data['scores'][i] > threshold: 
                class_ids.append(data['class_ids'][i])
                rois.append(data['rois'][i])
                scores.append(data['scores'][i])
                points.append(data['points'][i])
            
        predicted_data['class_ids'] = class_ids
        predicted_data['rois'] = rois
        predicted_data['scores'] = scores
        predicted_data['points'] = points
        
        # If there are no polygons above the DETECTION_MIN_CONFIDENCE threshold
        if len(predicted_data['class_ids']) == 0:
            # print("***** No polygon is above threshold {} *****".format(threshold))
            
            # ################# Handling no polygon detected scenario #########################
            # If nothing detected, updating the result files accordingly and moving to next image
            file_wise_df = pd.DataFrame([file_wise_data])
            if os.path.isfile(file_wise_data_fname):
                file_wise_df.to_csv(file_wise_data_fname, mode='a', header=False, index=False)
            else:
                file_wise_df.to_csv(file_wise_data_fname, mode='w', header=True, index=False)
        
            file_count += 1
            
            # ============  Saving the predicted masked Images   =======================
            if save_masked_image:
                # generating the labelled image
                raw_image = os.path.join(output_path,
                                         "raw_images",
                                         os.path.basename(data['image_location']))
                img = Image.open(raw_image)
                masked_image_name = os.path.join(output_path, 
                                                 "masked_images_predicted", 
                                                 os.path.basename(data['image_location']))
                predicted_image = draw_label(img, predicted_data, class_names)
                predicted_image.save(masked_image_name)
                       
            continue
            # ####################### Completed no class detected scenario #############################
        
        # #######################  when some detection happens #############################
        # predicted_data['class_ids'] = data['class_ids']
        for pred in predicted_data['class_ids']:
            class_wise_data['predicted_'+class_names[int(pred)]] += 1
            file_wise_data['predicted'] += 1
        
        # Generating masks from polygons
        predicted_data['masks'], points_to_remove_ = generate_mask_from_polygon(predicted_data['points'], 
                                                                                predicted_data['image_dimensions'][:2])
                
        # When some some file with 2 point polygon predicted encounters
        assert len(points_to_remove_) == 0, "Predicted file {} has 2 point polygon!".format(file)
        
        # computing the match of prediction with ground truth using the IoU_threshold
        gt_match, pred_match, _ = utils.compute_matches(np.array(curated_data['rois']),
                                                        np.array(curated_data['class_ids']),
                                                        np.array(curated_data['masks']),
                                                        np.array(predicted_data["rois"]),
                                                        np.array(predicted_data["class_ids"]),
                                                        np.array(predicted_data["scores"]),
                                                        np.array(predicted_data['masks']),
                                                        iou_threshold=IoU_threshold)
        # updating the overlap counts
        for gt in gt_match:
            if gt > 0:
                class_wise_data['overlap_curated_'+class_names[int(gt)]] += 1
                # file_wise_data['overlap_curated'] += 1
        for pred in pred_match:
            if pred > 0:
                class_wise_data['overlap_predicted_'+class_names[int(pred)]] += 1
                file_wise_data['overlap_polygons'] += 1
        
        # ============  Saving the predicted masked Images   =======================
        if save_masked_image:
            # generating the labelled image
            raw_image = os.path.join(output_path, 
                                                 "raw_images", 
                                                 os.path.basename(data['image_location']))
            img = Image.open(raw_image)
            masked_image_name = os.path.join(output_path, 
                                                 "masked_images_predicted", 
                                                 os.path.basename(data['image_location']))
            predicted_image = draw_label(img, predicted_data, class_names)
            predicted_image.save(masked_image_name)
        
        # computing the NOP values
        file_wise_data["non_overlapping_polygons"] = file_wise_data["predicted"] - file_wise_data["overlap_polygons"]
        
        # ==================  Updating the file wise data in csv   ================
        file_count += 1
        file_wise_df = pd.DataFrame([file_wise_data])
        if os.path.isfile(file_wise_data_fname):
            file_wise_df.to_csv(file_wise_data_fname, mode='a', header=False, index=False)
        else:
            file_wise_df.to_csv(file_wise_data_fname, mode='w', header=True, index=False)
    
    ## ========================= Calculating F-score ========================
    # Calculating and saving the classwise performance metrics to file
    
    df = pd.DataFrame([class_wise_data])
    df = df.transpose()
    df = df.sort_index()
    
    final_df = pd.concat([df[0:len(class_names)-1].reset_index(),\
        df[len(class_names)-1 : 2*(len(class_names)-1)].reset_index(),\
        df[2*(len(class_names)-1) : 3*(len(class_names)-1)].reset_index(),\
        df[3*(len(class_names)-1) : 4*(len(class_names)-1)].reset_index(),], axis=1)
    
    final_df.columns = ["curated", "curated_count", "curated_overlap", "curated_overlap_count", 
                        "predicted_overlap", "overlap_count", "predicted", "predicted_count"]
    final_df.drop(['curated_overlap', 'curated_overlap_count', 'predicted_overlap', 'predicted'], 
                  axis=1, inplace=True)
    final_df["non_overlap_count"] = final_df["predicted_count"] - final_df["overlap_count"]
    final_df["precision"] = final_df["overlap_count"] / final_df["predicted_count"]
    final_df["recall"] = final_df["overlap_count"] / final_df["curated_count"]
    final_df['Fscore'] = (2 * final_df["recall"] * final_df["precision"]) / (final_df["recall"] + final_df["precision"])
    
    final_df.to_csv(os.path.join(output_path, "final-fscore.csv"),
                    header=True, 
                    index=False)
    
    return final_df

def generate_mask_from_polygon(polygons, img_dim):
    '''
    Arguments:
        polygons - list of polygons (of length 'n')
        img_dim - dimension of the image (width X height)
    Returns:
        masks - a mask of dimension (width X height X n)
    
    '''
    masks = np.array([])
    count = 0
    points_to_remove_ = []
    for idx_to_rem, polygon in enumerate(polygons):
        if len(polygon) > 2:
            m1 = polygons_to_mask(list(img_dim), polygon)
            if count == 0:
                masks = np.expand_dims(m1, 2)
                count += 1
            else:
                masks = np.dstack((masks, m1))
        else:
            # print("2 point polygon detected...")
            points_to_remove_.append(idx_to_rem)
    return masks, points_to_remove_

def polygons_to_mask(img_shape, polygons):
    mask = np.ones(img_shape[:2], dtype=np.uint8)
    mask[mask < 255] = 0
    mask = PIL.Image.fromarray(mask)
    xy = list(map(tuple, polygons))
    PIL.ImageDraw.Draw(mask).polygon(xy=xy, outline=3, fill=1)
    mask = np.array(mask, dtype=bool)
    return mask

def load_image_gt(image_name, class_names):
    """Load and return ground truth data for an image (image, mask, bounding boxes).
    """
    # Load image and mask
    polygons, class_ids = load_polygon(image_name, class_names)
    bbox = extract_bbox_from_polygon(polygons)
    
    return class_ids, bbox

def extract_bbox_from_polygon(points):
    '''
    Exracting bounding box from list of polygons
    Argument:
        points: [list of polygon points] 
        point -> [[x1, y1], [x2, y2], ...]
    returns:
        bboxes: [list of bounding boxes]
        box-> [xmin, ymin, xmax, ymax]
    '''
    bboxes = []
    for _, point in enumerate(points):
        point = np.array(point)
        ymin, ymax = np.min(point[:, 0]), np.max(point[:, 0])
        xmin, xmax = np.min(point[:, 1]), np.max(point[:, 1])
        bbox = [xmin, ymin, xmax, ymax]
        bbox = [int(item) for item in bbox]
        bboxes.append(bbox)
    return bboxes

def load_polygon(image_name, class_names):
    """
    Getting the class_id and the polygon from curated json
    """
    
    class_name_to_id = {}
    class_id = 0
    for class_name in range(0, len(class_names)):
        class_name = class_names[class_name].strip()
        class_name_to_id[class_name] = class_id
        class_id += 1
    #===============================================================

    label_file = image_name.replace('.jpg', '.json')
    with open(label_file) as f:
        data = json.load(f)
        if len(data['shapes']) != 0:
            polygons, lbl = shapes_to_label(shapes=data['shapes'], 
                                            label_name_to_value=class_name_to_id)
        else: 
            polygons = np.array([])
            lbl = np.array([])    
    return polygons, lbl.astype(np.int32)

def shapes_to_label(shapes, label_name_to_value):
    polygons = []
    class_id = []
    for shape in shapes:
        polygons.append(shape['points'])
        cls_name = shape['label']
        cls_id = label_name_to_value[cls_name]
        class_id.append(cls_id)

    class_id = np.asarray(class_id)
    return polygons, class_id

if __name__ == '__main__':
    
    # loading the command line arguments
    args = vars(load_args())
    
    # loading the configuration file
    config = load_config(args['config'])
    # display_config(config)
    
    # updating the arguments from configuration
    args = update_args(args, config)
    
    # checking the evaluation mode
    class_type = config['yolo']['class_type']
    eval_mode = get_evaluation_mode(config)
    
    # loading the model
    model = YOLO(**args)
    
    if eval_mode == '0':
        #============ Precision recall calculation on the given dataset ===============#
        #TODO: precision recall
        pass
    elif eval_mode == '1':
        # getting the thresholds from configuration
        IoU_threshold = config['yolo']['IoU_threshold']
        thr = config['config_params']['eval_score_thresh']
        class_names = config['model_params'][class_type]['class_names']
        
        #============================== Evaluation on the given dataset ===============#
        # Setting the input and output directories
        input_path = config['model_params'][class_type]['dataset_path']
        input_path = os.path.join(input_path, 
                                  config['model_params'][class_type]['validation_dir'])
        output_path = config['model_params'][class_type]['output_path']
        output_path = os.path.join(output_path, 
                                   config['model_params'][class_type]['output_dir'])
        if not os.path.isdir(output_path):
            os.makedirs(output_path)
        
        #=============== generating data ==============================================#
        print("Detection on all images in {}".format(input_path))
        generate_data(model, input_path, output_path, class_names)
        
        #=============== Evaluation ===================================================#
        
        # evaluating the results
        print("Evaluating...")
        eval_df = evaluation(thr, class_names, output_path, IoU_threshold, True)
        print_separator("*")
        
        ## ================== Extracting and printing final metrics ===============
        # Reading the results from csv and sending the results to neptune
        curated_count = np.sum(eval_df['curated_count'].values)
        overlap_count = np.sum(eval_df['overlap_count'].values)
        non_overlap_count = np.sum(eval_df['non_overlap_count'].values)
        predicted_count = np.sum(eval_df['predicted_count'].values)
        precision = overlap_count / predicted_count
        recall = overlap_count / curated_count
        fscore = 2 * precision * recall / (precision + recall)
        
        print_separator("*")
        print("curated:{}, \tpredicted:{}, \tOP:{}, \tNOP:{}".format(curated_count,
                                                                     predicted_count, 
                                                                     overlap_count,
                                                                     non_overlap_count))
        print("Precision:{:.2f}, \tRecall:{:.2f}, \tF-Score:{:.2f}".format(precision * 100,
                                                                           recall * 100,
                                                                           fscore * 100))
        print_separator("*")
        
        print("Evaluation completed. Results are saved in {}".format(output_path))
        
    elif eval_mode == '2':
        class_names = config['model_params'][class_type]['class_names']
        #=================== Predicting on entire images in a directory ===============#
        # Setting the input and output directories
        input_path = config['model_params'][class_type]['dataset_path']
        input_path = os.path.join(input_path, 
                                  config['model_params'][class_type]['validation_dir'])
        output_path = config['model_params'][class_type]['output_path']
        output_path = os.path.join(output_path, 
                                   config['model_params'][class_type]['output_dir'])
        if not os.path.isdir(output_path):
            os.makedirs(output_path)
        
        print("Detection on all images in {}".format(input_path))
        # Detection
        predict_on_directory(model, input_path, output_path, class_names)
        print("Detection completed. Results are saved in {}".format(output_path))
        
    elif eval_mode == '3':
        #===================== Image detection on single image ========================#
        print("Detection for single image...")
        detected = predict_img(model, 
                               input_img_path=args['input'], 
                               output_img_path=args['output'])
        
        if detected:
            print("Detection completed. Result is saved in {}".format(args['output']))
            
    else:
        print("Incorrect configuration in config['evaluation']!")
    
    model.close_session()
    