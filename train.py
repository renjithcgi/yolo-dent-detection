"""
Retrain the YOLO model for your own dataset.
"""
import json
import argparse
import os
from glob import glob
from shutil import copy
import numpy as np
import keras.backend as K
from keras.layers import Input, Lambda
from keras.models import Model
from keras.optimizers import Adam
from keras.callbacks import TensorBoard, ModelCheckpoint, ReduceLROnPlateau, EarlyStopping

from yolo3.model import preprocess_true_boxes, yolo_body, yolo_loss
from yolo3.utils import get_random_data

print_separator = lambda x="#": print(x*100)

def _main():
    
    args = load_args()  # loading the arguments
    config = load_config(args.config)  # reading the config file
    
    # setting the path to directories from config file
    class_type = config['yolo']['class_type']
    train_annotation_path = config['model_params'][class_type]['train_annotation_path']
    test_annotation_path = config['model_params'][class_type]['test_annotation_path']
    
    log_dir = config['model_params'][class_type]['log_dir']
    classes_path = config['model_params'][class_type]['classes_path']
    anchors_path = config['model_params'][class_type]['anchors_path']
    load_pretrained = config['config_params']['load_pretrained']
    weights_path = args.weight
    
    class_names = get_classes(classes_path)
    num_classes = len(class_names)
    anchors = get_anchors(anchors_path)

    input_shape = (config['config_params']['input_shape'], 
                   config['config_params']['input_shape']) # multiple of 32, (h, w)

    model = create_model(input_shape, anchors, num_classes, load_pretrained=load_pretrained, 
                         freeze_body=2, weights_path=weights_path) # make sure you know what you freeze

    logging = TensorBoard(log_dir=log_dir)
    checkpoint = ModelCheckpoint(log_dir + 'ep{epoch:03d}-loss{loss:.3f}-val_loss{val_loss:.3f}.h5',
        monitor='val_loss', save_weights_only=True, save_best_only=True, period=1)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', 
                                  factor=0.2, 
                                  patience=config['config_params']['reduce_lr_patience'], 
                                  verbose=1)
    early_stopping = EarlyStopping(monitor='val_loss', 
                                   min_delta=0, 
                                   patience=config['config_params']['early_stopping_patience'], 
                                   verbose=1)

    with open(train_annotation_path) as f:
        train_files = f.readlines()
    num_train = len(train_files)
    
    with open(test_annotation_path) as f:
        test_files = f.readlines()
    num_test = len(test_files)
    

    # Train with frozen layers first, to get a stable loss.
    # Adjust num epochs to your dataset. This step is enough to obtain a not bad model.
    stage1_final_weights = os.path.join(log_dir, 'trained_weights_stage_1.h5')
    if config['config_params']['train_stage1']:
        lr = config['config_params']['stage1_learning_rate']
        model.compile(optimizer=Adam(lr=lr), loss={
            # use custom yolo_loss Lambda layer.
            'yolo_loss': lambda y_true, y_pred: y_pred})

        batch_size = config['config_params']['stage1_batch_size']
        print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_test, batch_size))
        model.fit_generator(data_generator_wrapper(train_files, batch_size, input_shape, anchors, num_classes),
                steps_per_epoch=max(1, num_train//batch_size),
                validation_data=data_generator_wrapper(test_files, batch_size, input_shape, anchors, num_classes),
                validation_steps=max(1, num_test//batch_size),
                epochs=config['config_params']['stage1_epochs'],
                initial_epoch=config['config_params']['stage1_initial_epochs'],
                callbacks=[logging, checkpoint, reduce_lr, early_stopping])
        
        # finding the best epoch weight
        weight_files = glob(log_dir + "/ep*.h5")
        weight_indices = [int(x.split("/")[-1].split("-")[0][2:]) for x in weight_files]
        best_weight_idx = np.argmax(weight_indices)
        copy(weight_files[best_weight_idx], stage1_final_weights)
        
    # Unfreeze and continue training, to fine-tune.
    # Train longer if the result is not good.
    if config['config_params']['train_stage2']:
        
        # If initial epoch is set to 0, ensuring that it is being continued from stage 1
        initial_epoch = config['config_params']['stage2_initial_epochs']
        model = create_model(input_shape, anchors, num_classes, load_pretrained=load_pretrained, 
                                freeze_body=2, weights_path=stage1_final_weights)
        for i in range(len(model.layers)):
            model.layers[i].trainable = True
        lr = config['config_params']['stage2_learning_rate']
        model.compile(optimizer=Adam(lr=lr), loss={'yolo_loss': lambda y_true, y_pred: y_pred}) # recompile to apply the change
        print('Unfreeze all of the layers.')

        batch_size = config['config_params']['stage2_batch_size'] # note that more GPU memory is required after unfreezing the body
        
        print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_test, batch_size))
        model.fit_generator(data_generator_wrapper(train_files, batch_size, input_shape, anchors, num_classes),
            steps_per_epoch=max(1, num_train//batch_size),
            validation_data=data_generator_wrapper(test_files, batch_size, input_shape, anchors, num_classes),
            validation_steps=max(1, num_test//batch_size),
            epochs=config['config_params']['stage2_epochs'],
            initial_epoch=initial_epoch,
            callbacks=[logging, checkpoint, reduce_lr, early_stopping])
        
        # finding the best epoch weight
        weight_files = glob(log_dir + "/ep*.h5")
        weight_indices = [int(x.split("/")[-1].split("-")[0][2:]) for x in weight_files]
        best_weight_idx = np.argmax(weight_indices)
        final_weights = os.path.join(log_dir, 'trained_weights_final.h5')
        copy(weight_files[best_weight_idx], final_weights)

    # Further training if needed.

def load_args():
    # class YOLO defines the default value, so suppress any default here
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    
    default_weight_path = "model_data/yolo_weights.h5"
    default_config_path = "YOLO_config.json"
    # Command line options
    parser.add_argument(
        '--weight', type=str, default=default_weight_path, 
        help='path to model weight file, default {}'.format(default_weight_path)
    )
    parser.add_argument(
        '--config', type=str, default=default_config_path, 
        help='path to configuration file, default {}'.format(default_config_path)
    )
    
    return parser.parse_args()

def load_config(config_path):
    '''
    Loads the yolo configuration for training or evalaution
    '''
    with open(config_path, 'r') as f:
        config = json.load(f)
    
    return config

def get_classes(classes_path):
    '''loads the classes'''
    with open(classes_path) as f:
        class_names = f.readlines()
    class_names = [c.strip() for c in class_names]
    return class_names

def get_anchors(anchors_path):
    '''loads the anchors from a file'''
    with open(anchors_path) as f:
        anchors = f.readline()
    anchors = [float(x) for x in anchors.split(',')]
    return np.array(anchors).reshape(-1, 2)

def create_model(input_shape, anchors, num_classes, load_pretrained=True, freeze_body=2,
            weights_path='model_data/yolo_weights.h5'):
    '''create the training model'''
    K.clear_session() # get a new session
    image_input = Input(shape=(None, None, 3))
    h, w = input_shape
    num_anchors = len(anchors)

    y_true = [Input(shape=(h//{0:32, 1:16, 2:8}[l], w//{0:32, 1:16, 2:8}[l], \
        num_anchors//3, num_classes+5)) for l in range(3)]

    model_body = yolo_body(image_input, num_anchors//3, num_classes)
    print('Create YOLOv3 model with {} anchors and {} classes.'.format(num_anchors, num_classes))

    if load_pretrained:
        model_body.load_weights(weights_path, by_name=True, skip_mismatch=True)
        print('Load weights {}.'.format(weights_path))
        if freeze_body in [1, 2]:
            # Freeze darknet53 body or freeze all but 3 output layers.
            num = (185, len(model_body.layers)-3)[freeze_body-1]
            for i in range(num): 
                model_body.layers[i].trainable = False
            print('Freeze the first {} layers of total {} layers.'.format(num, len(model_body.layers)))

    model_loss = Lambda(yolo_loss, output_shape=(1,), name='yolo_loss',
        arguments={'anchors': anchors, 'num_classes': num_classes, 'ignore_thresh': 0.5})(
        [*model_body.output, *y_true])
    model = Model([model_body.input, *y_true], model_loss)

    return model

def data_generator(annotation_lines, batch_size, input_shape, anchors, num_classes):
    '''data generator for fit_generator'''
    n = len(annotation_lines)
    i = 0
    while True:
        image_data = []
        box_data = []
        for b in range(batch_size):
            if i==0:
                np.random.shuffle(annotation_lines)
            image, box = get_random_data(annotation_lines[i], input_shape, random=True)
            image_data.append(image)
            box_data.append(box)
            i = (i+1) % n
        image_data = np.array(image_data)
        box_data = np.array(box_data)
        y_true = preprocess_true_boxes(box_data, input_shape, anchors, num_classes)
        yield [image_data, *y_true], np.zeros(batch_size)

def data_generator_wrapper(annotation_lines, batch_size, input_shape, anchors, num_classes):
    n = len(annotation_lines)
    if n == 0 or batch_size <= 0: 
        return None
    return data_generator(annotation_lines, batch_size, input_shape, anchors, num_classes)

if __name__ == '__main__':
    _main()
