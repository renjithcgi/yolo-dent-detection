import sys
import argparse
from yolo import YOLO, detect_video
from PIL import Image

def detect_img(yolo, input_, output_):
    img = input_
    try:
        image = Image.open(img)
    except:
        print('Open Error! Try again!')
        exit
    else:
        r_image = yolo.detect_image(image)
        r_image.show()
        r_image.save(output_)
    yolo.close_session()

FLAGS = None

if __name__ == '__main__':
    # class YOLO defines the default value, so suppress any default here
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    '''
    Command line options
    '''
    parser.add_argument(
        '--model', type=str,
        help='path to model weight file, default ' + YOLO.get_defaults("model_path")
    )

    parser.add_argument(
        '--anchors', type=str,
        help='path to anchor definitions, default ' + YOLO.get_defaults("anchors_path")
    )

    parser.add_argument(
        '--classes', type=str,
        help='path to class definitions, default ' + YOLO.get_defaults("classes_path")
    )

    parser.add_argument(
        '--gpu_num', type=int,
        help='Number of GPU to use, default ' + str(YOLO.get_defaults("gpu_num"))
    )

    FLAGS = parser.parse_args()
    
    '''
    Command line positional arguments -- for video detection mode
    '''
    parser.add_argument(
        "--input", nargs='?', type=str, required=False, default='./sample/input/1.jpg',
        help="Image input path"
    )

    parser.add_argument(
        "--output", nargs='?', type=str, required=False, default="./sample/output/temp.jpg",
        help="Image output path"
    )
    
    args = parser.parse_args()
    
    """
    Image detection mode, disregard any remaining command line arguments
    """
    print("Image detection mode...")
    if "input" in FLAGS:
        print(" Ignoring remaining command line arguments: " + FLAGS.input + "," + FLAGS.output)
    detect_img(YOLO(**vars(FLAGS)), input_=args.input, output_=args.output)
